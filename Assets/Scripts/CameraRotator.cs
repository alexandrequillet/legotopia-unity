﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotator : MonoBehaviour
{
    [SerializeField]
    private float m_speed;

    private float m_rotationX;
    private float m_rotationY;

    [SerializeField]
    private float m_fieldOfView = 10f;
    [SerializeField]
    private float m_minFieldOfView = 10f;
    [SerializeField]
    private float m_maxFieldOfView = 120f;

    private float m_zoomSpeed = 20f;

    void Update()
    {
        UpdateCameraRotation();
        UpdateCameraLookAt();
        UpdateCameraZoom();
    }

    void UpdateCameraRotation()
    {
        m_rotationX += Input.GetAxis("Mouse X") * m_speed * Time.deltaTime;
        m_rotationY += Input.GetAxis("Mouse Y") * m_speed * Time.deltaTime;
        m_rotationY = Mathf.Clamp(m_rotationY, 10, 90);

        transform.localEulerAngles = new Vector3(0, m_rotationX, m_rotationY);
    }

    void UpdateCameraLookAt()
    {
        Camera.main.transform.LookAt(new Vector3(0, 0, 0));
    }

    void UpdateCameraZoom()
    {
        m_fieldOfView += Input.GetAxis("Mouse ScrollWheel") * m_zoomSpeed;
        m_fieldOfView = Mathf.Clamp(m_fieldOfView, m_minFieldOfView, m_maxFieldOfView);
        Camera.main.fieldOfView = m_fieldOfView;
    }

    public void ZoomIn()
    {
        m_fieldOfView -= m_zoomSpeed/2;
        m_fieldOfView = Mathf.Clamp(m_fieldOfView, m_minFieldOfView, m_maxFieldOfView);
        Camera.main.fieldOfView = m_fieldOfView;
    }

    public void ZoomOut()
    {
        m_fieldOfView += m_zoomSpeed/2;
        m_fieldOfView = Mathf.Clamp(m_fieldOfView, m_minFieldOfView, m_maxFieldOfView);
        Camera.main.fieldOfView = m_fieldOfView;
    }
}
