﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JavascriptHook : MonoBehaviour
{
    [SerializeField]
    private DataSystem m_dataSystem;

    public void SetToken(string token)
    {
        m_dataSystem.SetToken(token);
    }
}
