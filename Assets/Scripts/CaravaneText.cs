﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CaravaneText : MonoBehaviour
{
    [SerializeField]
    private TextMeshPro m_text;


    public void SetText(string text)
    {
        m_text.SetText(text);
    }
}
