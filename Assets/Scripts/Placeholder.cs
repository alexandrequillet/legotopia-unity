﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Placeholder : MonoBehaviour
{
    [SerializeField]
    private Material m_yellowColor;
    [SerializeField]
    private Material m_blackColor;

    [SerializeField]
    private GameObject[] m_barrers;

    private bool m_isYellow = true;

    void Start()
    {
        StartCoroutine(ColorCycle());
    }

    IEnumerator ColorCycle() {

        while (true)
        {
            if (m_isYellow)
            {
                for (int i = 0; i < m_barrers.Length; i++)
                {
                    m_barrers[i].GetComponent<MeshRenderer>().material = m_blackColor;
                }
            }
            else
            {
                for (int i = 0; i < m_barrers.Length; i++)
                {
                    m_barrers[i].GetComponent<MeshRenderer>().material = m_yellowColor;
                }
            }

            yield return new WaitForSeconds(0.5f);

            m_isYellow = !m_isYellow;
        }

        
    }
}
