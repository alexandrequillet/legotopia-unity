﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class DataSystem : MonoBehaviour
{
    [SerializeField]
    private PropSystem m_propSystem;

    private string m_userToken = "0";
    private bool m_startVisualize = false;

    public void SetToken(string token)
    {
        m_userToken = token;
        m_startVisualize = true;
    }

    void Update()
    {
        if (m_startVisualize)
        {
            m_startVisualize = false;
            //StartCoroutine(GetRequest("http://192.93.212.212:8080/legotopia/get3DModel/"));
            StartCoroutine(GetRequest("http://192.93.212.212/unity/map2.json"));

            if (m_userToken != "0")
            {
                StartCoroutine(PostRequest("http://192.93.212.212:8080/legotopia/api/wish/new"));
            }
        }
    }

    IEnumerator GetRequest(string uri)
    {

        UnityWebRequest webRequest = UnityWebRequest.Get(uri);
        yield return webRequest.SendWebRequest();

        if (webRequest.isNetworkError || webRequest.isHttpError)
        {
            Debug.Log(webRequest.error);
        }
        else
        {
            Map map = JsonUtility.FromJson<Map>(webRequest.downloadHandler.text);
            ProcessGetData(map);
        }
    }

    IEnumerator PostRequest(string uri)
    {
        List<IMultipartFormSection> formData = new List<IMultipartFormSection>();
        formData.Add(new MultipartFormDataSection("Authorization Token " + m_userToken));

        UnityWebRequest webRequest = UnityWebRequest.Post(uri,formData);
        yield return webRequest.SendWebRequest();

        if (webRequest.isNetworkError || webRequest.isHttpError)
        {
            Debug.Log(webRequest.error);
        }
        else
        {
            Emplacement emplacement = JsonUtility.FromJson<Emplacement>(webRequest.downloadHandler.text);
            ProcessPostData(emplacement);
        }
    }

    void ProcessPostData(Emplacement emplacement)
    {
        m_propSystem.createEmplacement(new Vector3(emplacement.y, emplacement.z, emplacement.x));
    }

    void ProcessGetData(Map map)
    {
        m_propSystem.SetSizeOfMap(map.map_size.x, map.map_size.y);
        
        for(int i = 0; i < map.movings.Length; i++)
        {
            Moving caravane = map.movings[i];
            m_propSystem.createCaravane(new Vector3(caravane.location.pos_y, caravane.location.pos_z, caravane.location.pos_x),caravane.shape, caravane.name);
        }

        for (int i = 0; i < map.fixeds.Length; i++)
        {
            Fixed fix = map.fixeds[i];
            switch (fix.name) {
                case "Boucherie" :
                    m_propSystem.createButchery(new Vector3(fix.pos_y, fix.pos_z, fix.pos_x), fix.shape);
                break;
                case "Coiffeur":
                    m_propSystem.createHairdresser(new Vector3(fix.pos_y, fix.pos_z, fix.pos_x), fix.shape);
                    break;
                case "Boulangerie":
                    m_propSystem.createBakery(new Vector3(fix.pos_y, fix.pos_z, fix.pos_x), fix.shape);
                    break;
                case "Restaurant":
                    m_propSystem.createRestaurant(new Vector3(fix.pos_y, fix.pos_z, fix.pos_x), fix.shape);
                    break;
                case "Epicerie":
                    m_propSystem.createGrocery(new Vector3(fix.pos_y, fix.pos_z, fix.pos_x), fix.shape);
                    break;
                case "Bar":
                    m_propSystem.createBar(new Vector3(fix.pos_y, fix.pos_z, fix.pos_x), fix.shape);
                    break;
                case "Gare":
                    m_propSystem.createGare(new Vector3(fix.pos_y, fix.pos_z, fix.pos_x), fix.shape);
                    break;
                case "Ecole":
                    m_propSystem.createSchool(new Vector3(fix.pos_y, fix.pos_z, fix.pos_x), fix.shape);
                    break;
                case "Bureau1":
                    m_propSystem.createWork4x4(new Vector3(fix.pos_y, fix.pos_z, fix.pos_x), fix.shape);
                    break;
                case "Bureau2":
                    m_propSystem.createWork6x6(new Vector3(fix.pos_y, fix.pos_z, fix.pos_x), fix.shape);
                    break;
                case "Parc":
                    if(fix.shape.name == "Petit carre")
                    {
                        m_propSystem.createPark10x10(new Vector3(fix.pos_y, fix.pos_z, fix.pos_x), fix.shape);
                    } 
                    else if (fix.shape.name == "")
                    {
                        m_propSystem.createPark4x4(new Vector3(fix.pos_y, fix.pos_z, fix.pos_x), fix.shape);
                    }
                    break;
                
            }
        }
    }
}