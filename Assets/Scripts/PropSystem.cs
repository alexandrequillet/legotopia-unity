﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PropSystem : MonoBehaviour
{
    [SerializeField]
    private GameObject m_caravaneHPrefab;
    [SerializeField]
    private GameObject m_caravaneHExtPrefab;
    [SerializeField]
    private GameObject m_caravaneVPrefab;
    [SerializeField]
    private GameObject m_caravaneVExtPrefab;

    [SerializeField]
    private GameObject m_emplacementVPrefab;
    [SerializeField]
    private GameObject m_emplacementVExtPrefab;
    [SerializeField]
    private GameObject m_emplacementHPrefab;
    [SerializeField]
    private GameObject m_emplacementHExtPrefab;

    [SerializeField]
    private GameObject m_work2x2Prefab;
    [SerializeField]
    private GameObject m_work4x4Prefab;
    [SerializeField]
    private GameObject m_work6x6Prefab;

    [SerializeField]
    private GameObject m_park4x4Prefab;
    [SerializeField]
    private GameObject m_park10x10Prefab;

    [SerializeField]
    private GameObject m_garePrefab;
    [SerializeField]
    private GameObject m_schoolPrefab;

    [SerializeField]
    private GameObject m_butcheryPrefab;
    [SerializeField]
    private GameObject m_bakeryPrefab;
    [SerializeField]
    private GameObject m_hairdresserPrefab;
    [SerializeField]
    private GameObject m_groceryPrefab;
    [SerializeField]
    private GameObject m_restaurantPrefab;
    [SerializeField]
    private GameObject m_barPrefab;

    [SerializeField]
    private GameObject m_centre2x2Prefab;
    [SerializeField]
    private GameObject m_centre6x6Prefab;

    [SerializeField]
    private GameObject m_floorPlane;

    private Vector3 m_centerOfMap;

    public void SetSizeOfMap(int x, int y)
    {
        int tmp = x + 1;
        x = y + 1;
        y = tmp;
 
        m_centerOfMap = new Vector3(x / 2f, 0, y / 2f);
        m_floorPlane.transform.localScale = new Vector3(x / 8f, 0, y / 8f);
    }

    // CARAVANES

    public void createCaravane(Vector3 position, Shape shape, string name)
    {
        if (position.y == 0 && string.Equals(shape.name, "Rectangle H"))
        {
            CaravaneText caravane = Instantiate(m_caravaneHPrefab, position - m_centerOfMap, Quaternion.identity).GetComponent<CaravaneText>();
            caravane.SetText(name);
        } 
        else if (position.y > 0 && string.Equals(shape.name, "Rectangle H"))
        {
            CaravaneText caravane = Instantiate(m_caravaneHExtPrefab, position - m_centerOfMap, Quaternion.identity).GetComponent<CaravaneText>();
            caravane.SetText(name);
        }
        else if (position.y == 0 && string.Equals(shape.name, "Rectangle V"))
        {
            CaravaneText caravane = Instantiate(m_caravaneVPrefab, position - m_centerOfMap, Quaternion.identity).GetComponent<CaravaneText>();
            caravane.SetText(name);
        }
        else
        {
            CaravaneText caravane = Instantiate(m_caravaneVExtPrefab, position - m_centerOfMap, Quaternion.identity).GetComponent<CaravaneText>();
            caravane.SetText(name);
        }
    }

    // EMPLACEMENTS

    public void createEmplacement(Vector3 position)
    {
        Shape shape = new Shape();
        if (position.y == 0 && string.Equals(shape.name, "Rectangle H"))
        {
            Instantiate(m_emplacementHPrefab, position - m_centerOfMap, Quaternion.identity);
        }
        else if (position.y > 0 && string.Equals(shape.name, "Rectangle H"))
        {
            Instantiate(m_caravaneHExtPrefab, position - m_centerOfMap, Quaternion.identity);
        }
        else if (position.y == 0 && string.Equals(shape.name, "Rectangle V"))
        {
            Instantiate(m_emplacementVPrefab, position - m_centerOfMap, Quaternion.identity);
        }
        else
        {
            Instantiate(m_emplacementVExtPrefab, position - m_centerOfMap, Quaternion.identity);
        }
    }

    // WORK

    public void createWork2x2(Vector3 position, Shape shape)
    {
        Instantiate(m_work2x2Prefab, position - m_centerOfMap, Quaternion.identity);
    }

    public void createWork4x4(Vector3 position, Shape shape)
    {
        Instantiate(m_work4x4Prefab, position - m_centerOfMap, Quaternion.identity);
    }

    public void createWork6x6(Vector3 position, Shape shape)
    {
        Instantiate(m_work6x6Prefab, position - m_centerOfMap, Quaternion.identity);
    }

    // PARK

    public void createPark4x4(Vector3 position, Shape shape)
    {
        Instantiate(m_park4x4Prefab, position - m_centerOfMap, Quaternion.identity);
    }

    public void createPark10x10(Vector3 position, Shape shape)
    {
        Instantiate(m_park10x10Prefab, position - m_centerOfMap, Quaternion.identity);
    }

    // UNIQUE

    public void createSchool(Vector3 position, Shape shape)
    {
        Instantiate(m_schoolPrefab, position - m_centerOfMap, Quaternion.identity);
    }

    public void createGare(Vector3 position, Shape shape)
    {
        Instantiate(m_garePrefab, position - m_centerOfMap, Quaternion.identity);
    }

    // SERVICES   

    public void createButchery(Vector3 position, Shape shape)
    {
        Instantiate(m_butcheryPrefab, position - m_centerOfMap, Quaternion.identity);
    }

    public void createBakery(Vector3 position, Shape shape)
    {
        Instantiate(m_bakeryPrefab, position - m_centerOfMap, Quaternion.identity);
    }

    public void createHairdresser(Vector3 position, Shape shape)
    {
        Instantiate(m_hairdresserPrefab, position - m_centerOfMap, Quaternion.identity);
    }

    public void createGrocery(Vector3 position, Shape shape)
    {
        Instantiate(m_groceryPrefab, position - m_centerOfMap, Quaternion.identity);
    }

    public void createRestaurant(Vector3 position, Shape shape)
    {
        Instantiate(m_restaurantPrefab, position - m_centerOfMap, Quaternion.identity);
    }

    public void createBar(Vector3 position, Shape shape)
    {
        Instantiate(m_barPrefab, position - m_centerOfMap, Quaternion.identity);
    }

    // CENTRE

    public void createCentre2x2(Vector3 position, Shape shape)
    {
        Instantiate(m_centre2x2Prefab, position - m_centerOfMap, Quaternion.identity);
    }

    public void createCentre6x6(Vector3 position, Shape shape)
    {
        Instantiate(m_centre6x6Prefab, position - m_centerOfMap, Quaternion.identity);
    }
}
