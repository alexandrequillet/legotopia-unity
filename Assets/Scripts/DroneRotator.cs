﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneRotator : MonoBehaviour
{
    [SerializeField]
    private GameObject m_helice;
    [SerializeField]
    private int m_speed = 20;

    private float m_rotationY;

    // Update is called once per frame
    void Update()
    {
        m_rotationY += m_speed * Time.deltaTime;
        m_helice.transform.localEulerAngles = new Vector3(0, m_rotationY, 0);
    }
}
