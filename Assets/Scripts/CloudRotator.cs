﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudRotator : MonoBehaviour
{
    [SerializeField]
    private float m_speed;

    private float m_rotationX;

    // Update is called once per frame
    void Update()
    {
        m_rotationX += m_speed * Time.deltaTime;

        transform.localEulerAngles = new Vector3(0, m_rotationX, 0);
    }
}
