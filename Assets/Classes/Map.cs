﻿using UnityEngine;

[System.Serializable]
public class Map
{
    public MapSize map_size;

    public Moving[] movings;
    public Fixed[] fixeds;
}
