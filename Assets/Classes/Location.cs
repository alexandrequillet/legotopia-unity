﻿using UnityEngine;

[System.Serializable]
public class Location
{
    public int pos_x;
    public int pos_y;
    public int pos_z;
    public bool is_Available;
}