﻿using UnityEngine;

[System.Serializable]
public class Moving
{
    public string name;
    public Location location;
    public Shape shape;
}