﻿using UnityEngine;

[System.Serializable]
public class MapSize
{
    public int x;
    public int y;
}