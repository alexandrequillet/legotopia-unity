﻿using UnityEngine;

[System.Serializable]
public class Fixed
{
    public string name;
    public int pos_x;
    public int pos_y;
    public int pos_z;
    public Shape shape;
}