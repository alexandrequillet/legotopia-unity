﻿using UnityEngine;

[System.Serializable]
public class Shape
{
    public string name;
    public int length;
    public int width;
    public int height;
}